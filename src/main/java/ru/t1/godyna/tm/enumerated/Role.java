package ru.t1.godyna.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @Getter
    @NotNull
    private final String displayName;

    Role (@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
