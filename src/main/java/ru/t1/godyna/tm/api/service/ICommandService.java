package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.ICommandRepository;
import ru.t1.godyna.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService extends ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    AbstractCommand getCommandByArgument(String argument);

}
