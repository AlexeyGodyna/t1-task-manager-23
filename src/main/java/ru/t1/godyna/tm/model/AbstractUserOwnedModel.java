package ru.t1.godyna.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserOwnedModel extends AbstractModel {

    @Getter
    @Setter
    @Nullable
    private String userId;

}
