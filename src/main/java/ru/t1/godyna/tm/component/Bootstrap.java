package ru.t1.godyna.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.ICommandRepository;
import ru.t1.godyna.tm.api.repository.IProjectRepository;
import ru.t1.godyna.tm.api.repository.ITaskRepository;
import ru.t1.godyna.tm.api.repository.IUserRepository;
import ru.t1.godyna.tm.api.service.*;
import ru.t1.godyna.tm.command.AbstractCommand;
import ru.t1.godyna.tm.command.project.*;
import ru.t1.godyna.tm.command.system.*;
import ru.t1.godyna.tm.command.task.*;
import ru.t1.godyna.tm.command.user.*;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.godyna.tm.exception.system.CommandNotSupportedException;
import ru.t1.godyna.tm.model.Project;
import ru.t1.godyna.tm.repository.CommandRepository;
import ru.t1.godyna.tm.repository.ProjectRepository;
import ru.t1.godyna.tm.repository.TaskRepository;
import ru.t1.godyna.tm.repository.UserRepository;
import ru.t1.godyna.tm.service.*;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator{

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationListCommand());
        registry(new ApplicationVersionCommand());
        registry(new ApplicationHelpCommand());
        registry(new ArgumentListConnamd());
        registry(new SystemInfoCommand());
        registry(new ApplicationExitCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserRemoveCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    public void start(@Nullable final String[] args) {
        initLogger();
        initDemoData();
        if (args.length == 0 || args == null)
            runByCommand();
        else
            runByArgument(args);
    }

    private void runByArgument(@Nullable String[] args) {
        try {
            processArguments(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
        } finally {
            exit();
        }
    }

    private void runByCommand() {
        while(!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("TASK MANAGER HAS DONE");
            }
        });
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("user", "user", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.add(new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("DEMO PROJECT", Status.NOT_STARTED));
        projectService.add(new Project("BEST PROJECT", Status.IN_PROGRESS));
        projectService.add(new Project("BETA PROJECT", Status.COMPLETED));

        taskService.create("111","MEGA TASK");
        taskService.create("1111", "BETA TASK");
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

}
